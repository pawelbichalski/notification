<?php
/** @author Paweł Bichalski */

namespace Tests\Module\Notification\Unit\Application;

use App\Module\Notification\Application\Service\NotificationService;
use App\Module\Notification\Domain\Entity\UserNotificationConfig;
use App\Module\Notification\Domain\ValueObject\ContactChannel;
use App\Module\Notification\Infrastructure\Service\InMemoryEmailNotification;
use App\Module\Notification\Infrastructure\Service\InMemorySmsNotification;
use PHPUnit\Framework\TestCase;

class NotificationServiceTest extends TestCase
{
    /** @var NotificationService */
    private $serviceUnderTest;

    /** @var InMemoryEmailNotification */
    private $emailNotification;

    /** @var InMemorySmsNotification */
    private $smsNotification;

    public function setUp()
    {
        parent::setUp();

        $this->emailNotification = new InMemoryEmailNotification();
        $this->smsNotification = new InMemorySmsNotification();

        $this->serviceUnderTest = new NotificationService([
            $this->emailNotification,
            $this->smsNotification
        ]);
    }

    public function testShouldSendEmailNotificationWhenEmailWasSelected()
    {
        //Given
        $notificationConfig = new UserNotificationConfig('login1', 'email1@test.pl', [new ContactChannel(ContactChannel::EMAIL)]);

        //When
        $this->serviceUnderTest->send($notificationConfig, 'Test message 1');

        //Then
        $this->assertEquals('email1@test.pl', $this->emailNotification->getLastEmail());
        $this->assertEquals('Test message 1', $this->emailNotification->getLastMessage());
        $this->assertNull($this->smsNotification->getLastLogin());
        $this->assertNull($this->smsNotification->getLastMessage());
    }

    public function testShouldSendEmailAndSmsNotificationWhenEmailAndSmsWasSelected()
    {
        //Given
        $notificationConfig = new UserNotificationConfig(
            'login2',
            'email2@test.pl',
            [new ContactChannel(ContactChannel::EMAIL), new ContactChannel(ContactChannel::SMS)]
        );

        //When
        $this->serviceUnderTest->send($notificationConfig, 'Test message 2');

        //Then
        $this->assertEquals('email2@test.pl', $this->emailNotification->getLastEmail());
        $this->assertEquals('Test message 2', $this->emailNotification->getLastMessage());
        $this->assertEquals('login2', $this->smsNotification->getLastLogin());
        $this->assertEquals('Test message 2', $this->smsNotification->getLastMessage());
    }

    public function testShouldSendEmailNotificationWhenNoneWasSelected()
    {
        //Given
        $notificationConfig = new UserNotificationConfig('login3', 'email3@test.pl', null);

        //When
        $this->serviceUnderTest->send($notificationConfig, 'Test message 3');

        //Then
        $this->assertEquals('email3@test.pl', $this->emailNotification->getLastEmail());
        $this->assertEquals('Test message 3', $this->emailNotification->getLastMessage());
        $this->assertNull($this->smsNotification->getLastLogin());
        $this->assertNull($this->smsNotification->getLastMessage());
    }

    public function testShouldSendSmsNotificationWhenSmsWasSelected()
    {
        //Given
        $notificationConfig = new UserNotificationConfig('login4', 'email4@test.pl', [new ContactChannel(ContactChannel::SMS)]);

        //When
        $this->serviceUnderTest->send($notificationConfig, 'Test message 4');

        //Then
        $this->assertNull($this->emailNotification->getLastEmail());
        $this->assertNull($this->emailNotification->getLastMessage());
        $this->assertEquals('login4', $this->smsNotification->getLastLogin());
        $this->assertEquals('Test message 4', $this->smsNotification->getLastMessage());
    }
}
