<?php
/** @author Paweł Bichalski */
namespace Tests\Module\Notification\Unit\Application;

use App\Module\Notification\Application\Command\SendNotification;
use App\Module\Notification\Application\CommandHandler\SendNotificationHandler;
use App\Module\Notification\Application\Service\NotificationService;
use App\Module\Notification\Domain\Entity\UserNotificationConfig;
use App\Module\Notification\Domain\ValueObject\ContactChannel;
use App\Module\Notification\Infrastructure\Repository\InMemoryUserNotificationConfigRepository;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;


class SendNotificationHandlerTest extends TestCase
{
    /** @var InMemoryUserNotificationConfigRepository */
    private $repository;

    /** @var NotificationService|MockObject */
    private $notificationService;

    /** @var SendNotificationHandler */
    private $handlerUnderTest;

    public function setUp()
    {
        parent::setUp();

        $this->repository = new InMemoryUserNotificationConfigRepository();

        $this->notificationService = $this->getMockBuilder(NotificationService::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->repository->add(new UserNotificationConfig(
            'user_1',
            'user1@test.com',
            [new ContactChannel(ContactChannel::EMAIL)]
        ));
        $this->repository->add(new UserNotificationConfig(
            'user_2',
            'user2@test.com',
            [new ContactChannel(ContactChannel::EMAIL), new ContactChannel(ContactChannel::SMS)]
        ));

        $this->handlerUnderTest = new SendNotificationHandler($this->repository, $this->notificationService);
    }

    public function testShouldThrowExceptionWhenUserNotFound()
    {
        //Given
        $command = new SendNotification('non_exist_login', 'message');

        $handler = $this->handlerUnderTest;

        //When & then
        $this->expectException('RuntimeException');
        $this->expectExceptionMessage('User not found');
        $handler($command);
    }

    public function testShouldSendEmailNotificationWhenUserExists()
    {
        //Given
        $command = new SendNotification('user_1', 'Test message');

        $handler = $this->handlerUnderTest;

        //When & then
        $this->notificationService
            ->expects($this->once())
            ->method('send')
            ->with($this->repository->find('user_1'), 'Test message');
        $handler($command);
    }
}
