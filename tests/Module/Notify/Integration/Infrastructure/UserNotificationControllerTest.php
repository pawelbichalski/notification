<?php
/** @author Paweł Bichalski */

namespace Tests\Module\Notification\Integration\Infrastructure;

use App\Module\Notification\Infrastructure\Service\InMemoryEmailNotification;
use App\Module\Notification\Infrastructure\Service\InMemorySmsNotification;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class UserNotificationControllerTest extends WebTestCase
{
    public function setUp()
    {
        parent::setUp();
    }

    public function testShouldReturnUsers()
    {
        //Given
        $client = static::createClient();

        //When
        $client->xmlHttpRequest('GET', '/user-notification');

        //Then
        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());
        $response = \Safe\json_decode($client->getResponse()->getContent(), true);
        $this->assertArrayHasKey('login', $response[0]);
        $this->assertArrayHasKey('email', $response[0]);
        $this->assertArrayHasKey('contactChannels', $response[0]);
    }

    public function testShouldSendNotificationWhenUserExists()
    {
        //Given
        $client = static::createClient();
        $json = \Safe\json_encode(['message' => 'test']);

        //When
        $client->xmlHttpRequest('POST', '/user-notification/jan.kowalski', [], [], ['CONTENT_TYPE' => 'application/json'], $json);

        //Then
        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());
        /** @var InMemoryEmailNotification $emailService */
        $emailNotification = $client->getContainer()->get(InMemoryEmailNotification::class);
        /** @var InMemorySmsNotification $smsNotification */
        $smsNotification = $client->getContainer()->get(InMemorySmsNotification::class);
        $this->assertEquals('jan.kowalski@test.pl', $emailNotification->getLastEmail());
        $this->assertEquals('test', $emailNotification->getLastMessage());
        $this->assertNull($smsNotification->getLastLogin());
        $this->assertNull($smsNotification->getLastMessage());
    }
}
