<?php

namespace App\Module\Notification\Domain\Repository;

use App\Module\Notification\Domain\Entity\UserNotificationConfig;

/** @author Paweł Bichalski */

interface UserNotificationConfigRepository
{
    public function find(string $login): ?UserNotificationConfig;

    /**
     * @return UserNotificationConfig[]
     */
    public function findAll(): array;
}
