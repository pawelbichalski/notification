<?php
/** @author Paweł Bichalski */

namespace App\Module\Notification\Domain\ValueObject;

class ContactChannel
{
    const EMAIL = 'email';
    const SMS = 'sms';

    /** @var string */
    private $channel;

    public function __construct(string $channel)
    {
        if (!in_array($channel, self::supportedChannels())) {
            throw new \UnexpectedValueException('Channel not supported');
        }
        $this->channel = $channel;
    }

    /**
     * @return string[]
     */
    public static function supportedChannels(): array
    {
        return [
            self::EMAIL,
            self::SMS,
        ];
    }

    public function getChannel(): string
    {
        return $this->channel;
    }
}
