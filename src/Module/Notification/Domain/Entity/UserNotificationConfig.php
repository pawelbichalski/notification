<?php
/** @author Paweł Bichalski */
namespace App\Module\Notification\Domain\Entity;

use App\Module\Notification\Domain\ValueObject\ContactChannel;

class UserNotificationConfig
{
    /** @var string */
    private $login;

    /** @var string */
    private $email;

    /** @var ContactChannel[]|null */
    private $contactChannels;

    public function __construct(string $login, string $email, ?array $contactChannels)
    {
        $this->login = $login;
        $this->email = $email;
        $this->contactChannels = $contactChannels;
    }

    public function getLogin(): string
    {
        return $this->login;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getContactChannels(): ?array
    {
        return $this->contactChannels;
    }
}
