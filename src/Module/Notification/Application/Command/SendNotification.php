<?php
/** @author Paweł Bichalski */

namespace App\Module\Notification\Application\Command;

class SendNotification
{
    /** @var string */
    private $login;

    /** @var string */
    private $message;

    public function __construct(string $login, string $message)
    {
        $this->login = $login;
        $this->message = $message;
    }

    public function getLogin(): string
    {
        return $this->login;
    }

    public function getMessage(): string
    {
        return $this->message;
    }
}
