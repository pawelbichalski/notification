<?php
/** @author Paweł Bichalski */

namespace App\Module\Notification\Application\Service;

use App\Module\Notification\Domain\Entity\UserNotificationConfig;
use App\Module\Notification\Domain\ValueObject\ContactChannel;

class NotificationService
{
    /** @var iterable */
    private $transports;

    public function __construct(iterable $transports)
    {
        $this->transports = $transports;
    }

    public function send(UserNotificationConfig $notificationConfig, string $message)
    {
        $channels = $notificationConfig->getContactChannels() ?? [new ContactChannel(ContactChannel::EMAIL)];
        foreach ($channels as $channel) {
            foreach ($this->transports as $transport) {
                /** @var NotificationTransportInterface $transport */
                if ($transport->isSupported($channel)) {
                    $transport->send($notificationConfig, $message);
                }
            }
        }
    }
}
