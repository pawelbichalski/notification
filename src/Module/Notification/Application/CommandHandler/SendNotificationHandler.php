<?php
/** @author Paweł Bichalski */

namespace App\Module\Notification\Application\CommandHandler;

use App\Module\Notification\Application\Command\SendNotification;
use App\Module\Notification\Application\Service\NotificationService;
use App\Module\Notification\Domain\Repository\UserNotificationConfigRepository;

class SendNotificationHandler
{
    /** @var UserNotificationConfigRepository */
    private $repository;

    /** @var NotificationService */
    private $notificationService;

    public function __construct(UserNotificationConfigRepository $repository, NotificationService $notificationService)
    {
        $this->repository = $repository;
        $this->notificationService = $notificationService;
    }

    public function __invoke(SendNotification $command): void
    {
        $notificationConfig = $this->repository->find($command->getLogin());
        if (null === $notificationConfig) {
            throw new \RuntimeException('User not found', 0);
        }

        $this->notificationService->send($notificationConfig, $command->getMessage());
    }
}
