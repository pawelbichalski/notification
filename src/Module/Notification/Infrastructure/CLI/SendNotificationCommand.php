<?php
/** @author Paweł Bichalski */

namespace App\Module\Notification\Infrastructure\CLI;

use App\Module\Notification\Application\Command\SendNotification;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputDefinition;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Messenger\MessageBusInterface;

class SendNotificationCommand extends Command
{
    /** @var MessageBusInterface $bus */
    private $commandBus;

    protected static $defaultName = 'send-notification';

    /**
     * SendNotificationCommand constructor.
     * @param MessageBusInterface $commandBus
     */
    public function __construct(MessageBusInterface $commandBus)
    {
        parent::__construct();
        $this->commandBus = $commandBus;
    }

    protected function configure()
    {
        $this
            ->setDescription('Send notification to user')
            ->setDefinition(
                new InputDefinition([
                    new InputOption('message', 'm', InputOption::VALUE_REQUIRED),
                    new InputOption('user', 'u', InputOption::VALUE_REQUIRED)
                ])
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $command = new SendNotification($input->getOption('user'), $input->getOption('message'));
        $this->commandBus->dispatch($command);
        $output->writeln("Message was sent");

        return Command::SUCCESS;
    }
}
