<?php
/** @author Paweł Bichalski */

namespace App\Module\Notification\Infrastructure\Service;

use App\Module\Notification\Domain\Entity\UserNotificationConfig;
use App\Module\Notification\Domain\ValueObject\ContactChannel;

interface InMemoryNotificationTransportInterface
{
   public function isSupported(ContactChannel $contactChannel): bool;

   public function send(UserNotificationConfig $notificationConfig, string $message): void;
}
