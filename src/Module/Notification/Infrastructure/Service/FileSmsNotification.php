<?php
/** @author Paweł Bichalski */

namespace App\Module\Notification\Infrastructure\Service;

use App\Module\Notification\Application\Service\NotificationTransportInterface;
use App\Module\Notification\Domain\Entity\UserNotificationConfig;
use App\Module\Notification\Domain\ValueObject\ContactChannel;

class FileSmsNotification implements NotificationTransportInterface
{
    /** @var string */
    private $path;

    public function __construct(string $smsNotificationFilePath)
    {
        $this->path = $smsNotificationFilePath;
    }

    public function isSupported(ContactChannel $contactChannel): bool
    {
        return ContactChannel::SMS === $contactChannel->getChannel();
    }

    public function send(UserNotificationConfig $notificationConfig, string $message): void
    {
        $line = sprintf(
            "[%s] sending SMS notification to: %s, message: %s\n",
            (new \DateTime())->format('Y-m-d H:i:s'),
            $notificationConfig->getLogin(),
            $message
        );
        \safe\file_put_contents($this->path, $line, FILE_APPEND);
    }
}
