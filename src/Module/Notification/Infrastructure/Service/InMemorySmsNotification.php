<?php
/** @author Paweł Bichalski */

namespace App\Module\Notification\Infrastructure\Service;

use App\Module\Notification\Domain\Entity\UserNotificationConfig;
use App\Module\Notification\Domain\ValueObject\ContactChannel;

class InMemorySmsNotification implements InMemoryNotificationTransportInterface
{
    /** @var string|null */
    private $lastLogin;

    /** @var string|null */
    private $lastMessage;

    public function sendNotification(string $login, string $message): void
    {
        $this->lastLogin = $login;
        $this->lastMessage = $message;
    }

    public function getLastLogin(): ?string
    {
        return $this->lastLogin;
    }

    public function getLastMessage(): ?string
    {
        return $this->lastMessage;
    }

    public function isSupported(ContactChannel $contactChannel): bool
    {
        return ContactChannel::SMS === $contactChannel->getChannel();
    }

    public function send(UserNotificationConfig $notificationConfig, string $message): void
    {
        $this->lastLogin = $notificationConfig->getLogin();
        $this->lastMessage = $message;
    }
}
