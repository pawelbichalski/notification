<?php
/** @author Paweł Bichalski */

namespace App\Module\Notification\Infrastructure\Service;

use App\Module\Notification\Domain\Entity\UserNotificationConfig;
use App\Module\Notification\Domain\ValueObject\ContactChannel;

class InMemoryEmailNotification implements InMemoryNotificationTransportInterface
{
    /** @var string|null */
    private $lastEmail;

    /** @var string|null */
    private $lastMessage;

    public function getLastEmail(): ?string
    {
        return $this->lastEmail;
    }

    public function getLastMessage(): ?string
    {
        return $this->lastMessage;
    }

    public function isSupported(ContactChannel $contactChannel): bool
    {
        return ContactChannel::EMAIL === $contactChannel->getChannel();
    }

    public function send(UserNotificationConfig $notificationConfig, string $message): void
    {
        $this->lastEmail = $notificationConfig->getEmail();
        $this->lastMessage = $message;
    }
}
