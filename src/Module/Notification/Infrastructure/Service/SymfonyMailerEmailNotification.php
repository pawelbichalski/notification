<?php
/** @author Paweł Bichalski */

namespace App\Module\Notification\Infrastructure\Service;

use App\Module\Notification\Application\Service\NotificationTransportInterface;
use App\Module\Notification\Domain\Entity\UserNotificationConfig;
use App\Module\Notification\Domain\ValueObject\ContactChannel;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;

class SymfonyMailerEmailNotification implements NotificationTransportInterface
{
    /** @var MailerInterface */
    private $mailer;

    /** @var string */
    private $fromEmail;

    public function __construct(MailerInterface $mailer, string $mailerFromEmail)
    {
        $this->mailer = $mailer;
        $this->fromEmail = $mailerFromEmail;
    }

    public function isSupported(ContactChannel $contactChannel): bool
    {
        return ContactChannel::EMAIL === $contactChannel->getChannel();
    }

    public function send(UserNotificationConfig $notificationConfig, string $message): void
    {
        $email = (new Email())
            ->from($this->fromEmail)
            ->to($notificationConfig->getEmail())
            ->subject('Notification')
            ->text($message);
        $this->mailer->send($email);
    }
}
