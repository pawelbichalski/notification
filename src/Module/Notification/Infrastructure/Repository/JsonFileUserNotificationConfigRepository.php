<?php
/** @author Paweł Bichalski */

namespace App\Module\Notification\Infrastructure\Repository;

use App\Module\Notification\Domain\Entity\UserNotificationConfig;
use App\Module\Notification\Domain\Repository\UserNotificationConfigRepository;
use App\Module\Notification\Domain\ValueObject\ContactChannel;
use Safe\Exceptions\FilesystemException;
use Safe\Exceptions\JsonException;

class JsonFileUserNotificationConfigRepository implements UserNotificationConfigRepository
{
    /** @var string */
    private $filePath;

    public function __construct(string $dbJsonfilePath)
    {
        $this->filePath = $dbJsonfilePath;
    }

    public function find(string $login): ?UserNotificationConfig
    {
        foreach ($this->findAll() as $notificationConfig) {
            if ($login === $notificationConfig->getLogin()) {
                return $notificationConfig;
            }
        }
        return null;
    }

    /**
     * @return UserNotificationConfig[]
     */
    public function findAll(): array
    {
        try {
            $json = \safe\file_get_contents($this->filePath);
        } catch (FilesystemException $e) {
            throw new \UnexpectedValueException('Unable to read json file', $e->getCode(), $e);
        }
        try {
            $records = \safe\json_decode($json, true);
        } catch (JsonException $e) {
            throw new \UnexpectedValueException('Unable to decode json file', $e->getCode(), $e);
        }
        return array_map([$this, 'mapToEntity'], $records);
    }

    /**
     * @param mixed[] $data
     * @return UserNotificationConfig
     */
    private function mapToEntity(array $data): UserNotificationConfig
    {
        $channels = null;
        if (null !== $data['contact_channels']) {
            $channels = array_map(
                function (string $channel) {
                    return new ContactChannel($channel);
                },
                explode(',', $data['contact_channels'])
            );
        }
        return new UserNotificationConfig(
            $data['login'],
            $data['email'],
            $channels
        );
    }
}
