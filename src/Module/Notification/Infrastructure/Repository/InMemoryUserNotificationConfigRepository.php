<?php
/** @author Paweł Bichalski */

namespace App\Module\Notification\Infrastructure\Repository;

use App\Module\Notification\Domain\Entity\UserNotificationConfig;
use App\Module\Notification\Domain\Repository\UserNotificationConfigRepository;

class InMemoryUserNotificationConfigRepository implements UserNotificationConfigRepository
{
    /** @var UserNotificationConfig */
    private $store = [];

    public function add(UserNotificationConfig $notificationConfig): void
    {
        $this->store[$notificationConfig->getLogin()] = $notificationConfig;
    }

    public function find(string $login): ?UserNotificationConfig
    {
        return $this->store[$login] ?? null;
    }

    public function findAll(): array
    {
        return $this->store;
    }
}
