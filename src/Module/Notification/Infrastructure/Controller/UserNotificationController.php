<?php
/** @author Paweł Bichalski */

namespace App\Module\Notification\Infrastructure\Controller;

use App\Module\Notification\Application\Command\SendNotification;
use App\Module\Notification\Domain\Repository\UserNotificationConfigRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;

class UserNotificationController extends AbstractController
{
    /**
     * @Route("/user-notification", methods={"GET"})
     * @param UserNotificationConfigRepository $repository
     * @return JsonResponse
     */
    public function index(UserNotificationConfigRepository $repository): JsonResponse
    {
        return $this->json($repository->findAll());
    }

    /**
     * @Route("/user-notification/{login}", methods={"POST"})
     * @param string $login
     * @param MessageBusInterface $commandBus
     * @param Request $request
     * @return JsonResponse
     */
    public function send(string $login, MessageBusInterface $commandBus, Request $request): JsonResponse
    {
        $message = $request->get('message');
        if(empty($message)) {
            return $this->json('Please specify message', 422);
        }
        $commandBus->dispatch(new SendNotification($login, $message));

        return $this->json([]);
    }
}
